class TheTimeIs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date()};
  }
    // componentDidMount() always runs after component output has been rendered
    componentDidMount() {
      this.timerID = setInterval(() => this.tick(),1000);

  }
  //componentWillUnmount() runs if the component is ever removed from the DOM
  componentWillUnmount() {
    clearInterval(this.timerID);

  }

  render() {
    return <span>The time is {this.state.date.toLocaleTimeString()}</span>;
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }
}