import React from 'react';
import ReactDOM from 'react-dom';

class Hello extends React.Component {
  render() {
  	return (
      <p>Hello world!</p>
    );
  }
}


// Must export!
export default Hello;