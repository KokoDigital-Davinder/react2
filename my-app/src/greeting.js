import React from 'react';
import ReactDOM from 'react-dom';
import Hello from './greeting/hello.js';
import Goodbye from './greeting/goodbye.js';

class Greeting extends React.Component {
  render() {
  	return (
  		<div className="greeting">
      		<Hello/>
      		<Goodbye/>
      	</div>
    );
  }
}


// Must export!
export default Greeting;