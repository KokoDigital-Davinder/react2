import React from 'react';
import ReactDOM from 'react-dom';
import Note from './greeting/note.js';

class Goodbye extends React.Component {
  render() {
  	return (
  	<div className="goodbye">
  		<p>Goodbye world!</p>
  		<Note/>
  	</div>
    );
  }
}


// Must export!
export default Goodbye;